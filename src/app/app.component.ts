import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'Players';
	PlayersList=  [
	{id: 0, name: 'Dhoni',field:[],default:"wk"},
	{id: 1, name: 'Virat',field:[],default:"allRounder"},
	{id: 2, name: 'Aswin',field:[],default:"batsMan"},
	{id: 3, name: 'Rayudu',field:[],default:"bowler"},
	{id: 4, name: 'Raina',field:[],default:"allRounder"},
	{id: 5, name: 'Bhuvaneshkar',field:[],default:"bowler"},
	{id: 6, name: 'harbageen',field:[],default:"bowler"},
	{id: 7, name: 'Yuvraj',field:[],default:"batsMan"}
	];
	bowlersList:any=[];
	allRowndersList:any=[];
	batsman:any=[];
	wicketKeeper:any=[];
	viewPlayersList = [
	{id: 0, name: 'Dhoni',field:[],default:"wk"},
	{id: 1, name: 'Virat',field:[],default:"allRounder"},
	{id: 2, name: 'Aswin',field:[],default:"batsMan"},
	{id: 3, name: 'Rayudu',field:[],default:"bowler"},
	{id: 4, name: 'Raina',field:[],default:"allRounder"},
	{id: 5, name: 'Bhuvaneshkar',field:[],default:"bowler"},
	{id: 6, name: 'harbageen',field:[],default:"bowler"},
	{id: 7, name: 'Yuvraj',field:[],default:"batsMan"}
	];

	constructor(){}

	selectPlayer(name,id){
		let index = this.PlayersList.findIndex(item => item.id === id);
		let viewIndex = this.viewPlayersList.findIndex(item => item.id === id);
		if(this.PlayersList[index].field.findIndex(item => item == this.PlayersList[index].default) < 0){
			if(this.PlayersList[index].default == "wk"){
				this.wicketKeeper.push({id,name})
				this.PlayersList[index].field.push("wk");
			} else if(this.PlayersList[index].default == "bowler"){
				this.bowlersList.push({id,name})
				this.PlayersList[index].field.push("bowler");
			} else if(this.PlayersList[index].default == "allRounder"){
				this.allRowndersList.push({id,name})
				this.PlayersList[index].field.push("allRounder");
			} else if(this.PlayersList[index].default == "batsMan"){
				this.batsman.push({id,name})
				this.PlayersList[index].field.push("batsMan");
			}  
		} else{
			if(this.PlayersList[index].field.findIndex(item => item == "wk") < 0){
				this.wicketKeeper.push({id,name})
				this.PlayersList[index].field.push("wk");
			} else if(this.PlayersList[index].field.findIndex(item => item == "bowler") < 0){
				this.bowlersList.push({id,name})
				this.PlayersList[index].field.push("bowler");
			} else if(this.PlayersList[index].field.findIndex(item => item == "allRounder") < 0){
				this.allRowndersList.push({id,name})
				this.PlayersList[index].field.push("allRounder");
			} else if(this.PlayersList[index].field.findIndex(item => item == "batsMan") < 0){
				this.batsman.push({id,name})
				this.PlayersList[index].field.push("batsMan");
			} else {
				alert("Already set to all fields")
			}
		 }
		 this.viewPlayersList.splice(viewIndex,1);
	}

	removeList(id,field){
		let index = this.PlayersList.findIndex(item => item.id === id);
		this.viewPlayersList.push(this.PlayersList[index]);
		if(field == "wk"){
			let fieldIndex = this.wicketKeeper.findIndex(item => item.id === id)
			this.wicketKeeper.splice(fieldIndex,1);
		}
		else if(field == "bat"){
			let fieldIndex = this.batsman.findIndex(item => item.id === id)
			this.batsman.splice(fieldIndex,1);
		} else if(field == "all"){
			let fieldIndex = this.allRowndersList.findIndex(item => item.id === id)
			this.allRowndersList.splice(fieldIndex,1);
		} else if(field == "bow"){
			let fieldIndex = this.bowlersList.findIndex(item => item.id === id)
			this.bowlersList.splice(fieldIndex,1);
		}

	}
	
}
